**Note: This project is no longer maintained**

# Purpose
Periodically check for available appointments for COVID-19 vaccines using 
the https://www.vaccinespotter.org/ API. Available methods are stand-alone
Python script and [Telegram Bot](t.me/cv19spot_bot).

# Requirements
* Python 3.6+
* Requirements in `requirements.txt` (`pip install -r requirements.txt`)
* Telegram Bot Token (only if using Telegram Bot)
* Docker and Docker Compose (only if using Docker and Telegram Bot)

# Usage
See `python covid-appointments.py -h` for stand-alone script
or `python cv19spot-telegram-bot.py -h` for Telegram Bot.

Sample `Dockerfile` and `docker-compose.yaml` provided. Environment variables
need to be provided via `.env` file. Execute with `docker-compose up`.

# Todo
- [x] Make some bot data persistent so jobs restart without user interaction - 
      just need the data to recreate AppointmentFinder instance and create job 
      instance; user ids, chat ids, etc...
- [x] Store postal code with AppointmentFinder instead of coordinates
- [x] Have `/settings` query data from AppointmentFinder instance
- [ ] Settable message rate?
- [ ] Better error messaging to user when wrong setup data is entered
- [x] Log number of duplicate appointments and new appointments to make sure new
      appoiontments are really being captured
- [x] Appealling formatting of appointment messages
- [ ] Consider only saving basic data types for restarts - no complex objects
- [x] Exception handling that notifies user and developer of issue
- [ ] Regression testing
- [ ] Will this bot work with groups?
- [ ] Give user info about number of locations within different distances while asking
      for distance
- [x] Report number of active search jobs in `/status`
- [ ] Consider adding finer-grained search like vaccine type and appointment
      types/hours (limited data currently available, but could improve in future)
- [x] Be more lenient about input and smarter about parsing answers; e.g., allow
      case-insensitive State code input
- [ ] Attribute vaccinespotter.org but maybe avoid link in multiple places so user
      is not confused when links are shared (Bot About or Description)
- [ ] Developer channel specific commands to more bot info
- [ ] Timeout for search jobs? Make user re-subscribe?
- [ ] Add fun emojis to messages
- [ ] Sort appointments by distance
