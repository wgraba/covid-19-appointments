#!/usr/bin/env python3

import argparse
import dateutil
import time
import datetime
from cv19spot import *


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Periodically check for available vaccine appointments using vaccinespotter.org API",
    )
    parser.add_argument(
        "-d",
        "--distance",
        help="Distance in miles from Zip Code (-z) to limit search",
        type=int,
    )
    parser.add_argument(
        "-p",
        "--postalcode",
        help="Postal code to base search with --distance",
        type=int,
    )
    parser.add_argument(
        "-s",
        "--state-code",
        help="Specify the state code of the JSON from https://www.vaccinespotter.org/api/ ( default: CO )",
        default="CO",
        type=str,
    )
    args = parser.parse_args()

    state_data = vac_spotter.get_state_data(args.state_code)
    if state_data:
        print(
            f"State {state_data.state_code} has {state_data.num_locations} locations and {state_data.num_providers} providers!"
        )

    else:
        raise ValueError(
            f"State {args.state_code} doesn't exist or is not supported by https://vaccinespotter.org"
        )

    loc = SimpleGeocoder().get_loc(args.postalcode)
    if loc:
        print(
            f"Postal Code {args.postalcode:05d} is located at latitude {loc[0]} and longitude {loc[1]}"
        )

    else:
        raise ValueError(
            f"Unable to find location for Postal Code {args.postalcode:05d}"
        )

    print("\nGetting available appointments (CTRL-C to stop)...")
    appt_finder = AppointmentFinder(args.state_code, args.distance, args.postalcode)
    old_appts = []
    while True:
        try:
            new_appts = appt_finder.get_new_appts()
            if len(new_appts) <= 0:
                print(
                    f"No new appointments available; last checked {appt_finder.last_check_date.strftime('%H:%M:%S %Z')}"
                )
                pass

            else:
                for appt in new_appts:
                    print(f"\n{appt}")

            time.sleep(10)

        except KeyboardInterrupt:
            print("Quitting...")
            break
