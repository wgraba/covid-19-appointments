#! /usr/bin/env python3

import argparse
import logging
from typing import Union
import telegram as tg
import telegram.ext as tge
import cv19spot
import requests
import enum
import traceback
import html
import json

logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)


class ConversationState(enum.Enum):
    GET_STATE = (enum.auto(),)
    GET_POSTAL_CODE = (enum.auto(),)
    GET_DISTANCE = (enum.auto(),)
    START_LOOKING = (enum.auto(),)


class DataId(enum.Enum):
    STATE_CODE = (enum.auto(),)
    DISTANCE = (enum.auto(),)
    POSTAL_CODE = (enum.auto(),)
    APPT_FINDER = (enum.auto(),)
    USER = (enum.auto(),)
    CHAT_ID = (enum.auto(),)
    JOBS = (enum.auto(),)


APPT_POLL_PERIOD_S = 30  # Period in seconds to poll for new appointments
DEVELOPER_CHAT_ID = -581189595
TG_MSG_LIM_NUM_CHARS = 4096
TG_MSG_TOO_LONG = "\n----\nMESSAGE TRUNCATED"


class CovidVacBot:
    """
    Telegram Bot to query COVID-19 vaccine info from https://vaccinespotter.org

    :param updater: Updater bot abstraction instance
    """

    def __init__(self, updater: tge.updater.Updater) -> None:
        self._updater = updater

        conv_handler = tge.ConversationHandler(
            entry_points=[tge.CommandHandler("start", self.start)],
            states={
                ConversationState.GET_STATE: [
                    tge.MessageHandler(
                        tge.Filters.regex("[A-Za-z]{2}"), self._get_state
                    ),
                    tge.MessageHandler(
                        tge.Filters.text & (~tge.Filters.command), self._unknown_input
                    ),
                ],
                ConversationState.GET_POSTAL_CODE: [
                    tge.MessageHandler(
                        tge.Filters.regex("^[0-9]{5}$"), self._get_postal_code
                    ),
                    tge.MessageHandler(
                        tge.Filters.text & (~tge.Filters.command), self._unknown_input
                    ),
                ],
                ConversationState.GET_DISTANCE: [
                    tge.MessageHandler(
                        tge.Filters.regex("^[0-9.]+$"), self._get_distance
                    ),
                    tge.MessageHandler(
                        tge.Filters.text & (~tge.Filters.command), self._unknown_input
                    ),
                ],
            },
            fallbacks=[tge.CommandHandler("stop", self.stop)],
            allow_reentry=True,
        )
        self._updater.dispatcher.add_handler(conv_handler)
        self._updater.dispatcher.add_handler(tge.CommandHandler("help", self.help))
        self._updater.dispatcher.add_handler(
            tge.CommandHandler("settings", self.settings)
        )
        self._updater.dispatcher.add_handler(tge.CommandHandler("status", self.status))
        self._updater.dispatcher.add_handler(tge.CommandHandler("stop", self.stop))

        self._updater.dispatcher.add_error_handler(self._error_handler)

        self._commands = [
            tg.BotCommand("/help", "Get help"),
            tg.BotCommand("/settings", "Get Bot settings"),
            tg.BotCommand("/start", "Start Bot and add search job"),
            tg.BotCommand("/status", "Status of Bot and search job"),
            tg.BotCommand("/stop", "Stop Bot and search job"),
        ]

        self._updater.bot.set_my_commands(self._commands)

        # Restart jobs
        bot_data = self._updater.dispatcher.bot_data
        jobs = bot_data.get(DataId.JOBS.name, {})
        if len(jobs) > 0:
            logger.info("Found saved jobs; restarting....")

        new_jobs = {}
        for _, job in jobs.items():
            # TODO: Consider using User ID instead of Job ID as Key and Job name
            #       since it can be used with user data to restart jobs.
            #       Might need versioning to translate saved data structures?
            user = job[DataId.USER.name]
            chat_id = job[DataId.CHAT_ID.name]
            user_data = self._updater.dispatcher.user_data[user.id]

            state_code = user_data.get(DataId.STATE_CODE.name)
            postal_code = user_data.get(DataId.POSTAL_CODE.name)
            distance = user_data.get(DataId.DISTANCE.name)

            appt_finder = cv19spot.AppointmentFinder(state_code, distance, postal_code)
            user_data[DataId.APPT_FINDER.name] = appt_finder

            new_job = self._updater.job_queue.run_repeating(
                self._appt_update,
                APPT_POLL_PERIOD_S,
                context={"_id": chat_id, "user_data": user_data},
                name=str(user),
            )
            new_jobs[new_job.job.id] = {
                DataId.USER.name: user,
                DataId.CHAT_ID.name: chat_id,
            }

            logger.info(
                f"Finding appointments with settings {state_code} {postal_code:05d} {distance}mi"
            )

        bot_data[DataId.JOBS.name] = new_jobs

    def _error_handler(self, update: tg.Update, context: tge.CallbackContext):
        """
        Log the error and send a telegram message to notify the developer.
        """
        # Log the error before we do anything else, so we can see it even if something breaks.
        logger.error(msg="Exception while handling an update:", exc_info=context.error)

        # traceback.format_exception returns the usual python message about an exception, but as a
        # list of strings rather than a single string, so we have to join them together.
        tb_list = traceback.format_exception(
            None, context.error, context.error.__traceback__
        )
        tb_string = "".join(tb_list)

        # Build the message with some markup and additional information about what happened.
        # You might need to add some logic to deal with messages longer than the 4096 character limit.
        update_str = update.to_dict() if isinstance(update, tg.Update) else str(update)
        message = (
            f"An exception was raised while handling an update\n"
            f"<pre>update = {html.escape(json.dumps(update_str, indent=2, ensure_ascii=False))}"
            "</pre>\n\n"
            f"<pre>context.chat_data = {html.escape(str(context.chat_data))}</pre>\n\n"
            f"<pre>context.user_data = {html.escape(str(context.user_data))}</pre>\n\n"
            f"<pre>{html.escape(tb_string)}</pre>"
        )

        # Finally, send the message
        context.bot.send_message(
            chat_id=DEVELOPER_CHAT_ID, text=message, parse_mode=tg.ParseMode.HTML
        )

    # def cancel(self, update: tg.Update, context: tge.CallbackContext):
    #     """
    #     /cancel command to cancel setup

    #     :param update: Telegram update
    #     :param context: Update context
    #     """
    #     update.message.reply_text(
    #         "Cancelling setup...run /start if you want to try again"
    #     )

    #     return tge.ConversationHandler.END

    def help(self, update: tg.Update, context: tge.CallbackContext):
        """
        /help command to print help

        :param update: Telegram update
        :param context: Update context
        """
        msg_txt = (
            "cv19spot-bot uses the https://vacspotter.org API to help find open appointments in your area\n\n"
            "<u><b>Commands</b></u>"
        )
        for cmd in self._commands:
            msg_txt += f"\n{cmd.command} - {cmd.description}"

        update.message.reply_html(msg_txt)

    def settings(self, update: tg.Update, context: tge.CallbackContext):
        """
        /settings command to print settings

        :param update: Telegram update
        :param context: Update context
        """
        msg_txt = "Bot has not been setup - run /start command"
        if appt_finder := context.user_data.get(DataId.APPT_FINDER.name):
            state_code = appt_finder.state_code
            postal_code = appt_finder.postal_code
            distance = appt_finder.distance
            msg_txt = (
                f"Looking for appointments in the State of {state_code} within {distance}mi of Postal Code {postal_code}."
                "\n\n"
                "Send /start to modify"
            )

        msg_txt += (
            "\n\n"
            f"User ID: {update.effective_user.id}; Chat ID: {update.effective_chat.id}"
        )

        update.message.reply_text(msg_txt)

    def start(
        self, update: tg.Update, context: tge.CallbackContext
    ) -> ConversationState:
        """
        /start command to start bot

        :param update: Telegram update
        :param context: Update context
        :return: Next Conversation State
        """
        msg_text = (
            "Hello! I use the https://vacspotter.org API to find open appointments in your area. "
            "Send /stop at any time to stop vaccine searches or to stop setup."
            "\n\n"
            "What 2-digit State Code do you want to search? (e.g., CO)"
        )
        update.message.reply_text(msg_text)

        return ConversationState.GET_STATE

    def status(self, update: tg.Update, context: tge.CallbackContext):
        """
        /status command to get status of bot and polling

        :param update: Telegram update
        :param context: Update context
        """
        msg_txt = "No jobs - run /start command"
        if appt_finder := context.user_data.get(DataId.APPT_FINDER.name):
            state_code = appt_finder.state_code
            state_data = cv19spot.vac_spotter.get_state_data(state_code)
            postal_code = appt_finder.postal_code
            distance = appt_finder.distance
            mod_date = (
                appt_finder.last_check_date.strftime("%H:%M %Z")
                if appt_finder.last_check_date
                else "(has not checked yet)"
            )
            msg_txt = (
                f"Looking for appointments in the State of {state_data.state_code} within {distance}mi of Postal Code {postal_code:05d}. "
                f"State of {state_data.state_name} currently has {state_data.num_locations} locations and {state_data.num_providers} providers."
                "\n\n"
                "Send /start to modify"
                "\n\n"
                f"Last checked with https://vaccinespotter.org at {mod_date}."
            )

        num_jobs = len(context.job_queue.jobs())
        msg_txt += (
            "\n\n"
            f"There are currently {num_jobs} active search jobs across all users."
        )

        update.message.reply_text(msg_txt)

    def stop(self, update: tg.Update, context: tge.CallbackContext):
        """
        /stop command to stop all jobs

        :param update: Telegram update
        :param context: Update context
        """
        if context.job_queue and context.user_data:
            self._remove_jobs(
                context.job_queue,
                str(update.effective_user),
                context.bot_data,
                context.user_data,
            )

        msg_txt = (
            "Goodbye! I'll stop looking for appointments."
            "\n\n"
            "Use /start to restart appointment notifications."
        )
        update.message.reply_text(msg_txt)

        return tge.ConversationHandler.END

    def _unknown_input(self, update: tg.Update, context: tge.CallbackContext):
        """
        Message Handler for unknown input

        :param update: Telegram update
        :param context: Update context
        """
        user_input = update.message.text
        logger.info(f"User input '{user_input}' rejected!")
        update.message.reply_text(
            "Sorry, that input was unrecognized. Please try again."
        )

    def _get_state(
        self, update: tg.Update, context: tge.CallbackContext
    ) -> ConversationState:
        """
        Get State from user and ask next question

        :param update: Telegram update
        :param context: Update context
        :return: Next Conversation State
        """
        msg_txt = ""
        next_state = ConversationState.GET_STATE
        state_code = update.message.text.upper()

        state_data = cv19spot.vac_spotter.get_state_data(state_code)
        if state_data:
            context.user_data[DataId.STATE_CODE.name] = state_code
            msg_txt = (
                f"Got it - {state_data.state_code}!\n"
                f"State {state_data.state_name} has {state_data.num_locations} locations and {state_data.num_providers} providers.\n\n"
                "What 5-digit Postal Code do you want to use to calculate distance? (e.g., 80201)"
            )
            next_state = ConversationState.GET_POSTAL_CODE

        else:
            msg_txt = (
                f"Sorry - {state_code} is either not a recognized State Code or is not supported by https://vaccinespotter.org\n\n"
                "Please try again."
            )

            if state_code == "ZZ":
                msg_txt += (
                    "\n\nP.S. Maybe one day this will be the State Code for ZZ Top..."
                )

        update.message.reply_text(msg_txt)

        return next_state

    def _get_postal_code(
        self, update: tg.Update, context: tge.CallbackContext
    ) -> ConversationState:
        """
        Get Postal Code from user and ask next question

        :param update: Telegram update
        :param context: Update context
        :return: Next Conversation State
        """
        msg_txt = ""
        next_state = ConversationState.GET_POSTAL_CODE
        postal_code = int(update.message.text)

        loc = cv19spot.SimpleGeocoder().get_loc(postal_code)
        if loc:
            context.user_data[DataId.POSTAL_CODE.name] = postal_code
            next_state = ConversationState.GET_DISTANCE
            msg_txt = (
                f"Got it - {postal_code:05d}!\n"
                f"{postal_code:05d} is located at latitude {loc[0]:.2f} and longitude {loc[1]:.2f}\n\n"
                "What is the maximum distance in mi for your search? (e.g., 25)"
            )

        else:
            msg_txt = (
                f"Sorry - couldn't find Postal Code {postal_code:05d}\n\n"
                "Please try again."
            )
        update.message.reply_text(msg_txt)

        return next_state

    def _get_distance(self, update: tg.Update, context: tge.CallbackContext):
        """
        Get distance from user and start job to looks for open appointments

        :param update: Telegram update
        :param context: Update context
        :return: Next Conversation State
        """
        distance = float(update.message.text)
        context.user_data[DataId.DISTANCE.name] = distance

        msg_txt = f"Got it <{distance}mi!"

        if context.job_queue and update.effective_user and context.user_data:
            self._add_job(
                context.job_queue,
                update.effective_user,
                update.message.chat_id,
                context.bot_data,
                context.user_data,
            )

            appt_finder = context.user_data.get(DataId.APPT_FINDER.name)

            msg_txt += (
                "\n\n"
                f"I'll let you know when there are new open appointments in the State of {appt_finder.state_code} "
                f"within {appt_finder.distance}mi of {appt_finder.postal_code}!"
            )

        else:
            msg_txt += "\n\n" "Unknown error creating search job - please try again"

        if len(msg_txt) > 0:
            update.message.reply_text(msg_txt)

        return tge.ConversationHandler.END

    def _add_job(
        self,
        job_queue: tge.JobQueue,
        user: tg.User,
        chat_id: int,
        bot_data: dict,
        user_data: dict,
    ) -> Union[tge.Job, None]:
        """
        Add search job to queue.

        Only one search job is allowed per user.

        :param job_queue: Job Queue to use
        :param user: User requesting search
        :param chat_id: ID of chat requesting search
        :bot_data: Bot Data - used for persistnce of jobs across reboots
        :user_data: Data of user requesting search
        """
        job = None
        state_code = user_data.get(DataId.STATE_CODE.name)
        postal_code = user_data.get(DataId.POSTAL_CODE.name)
        distance = user_data.get(DataId.DISTANCE.name)

        if state_code:
            job_name = str(user)
            self._remove_jobs(job_queue, job_name, bot_data, user_data)

            appt_finder = cv19spot.AppointmentFinder(state_code, distance, postal_code)
            user_data[DataId.APPT_FINDER.name] = appt_finder

            job = job_queue.run_repeating(
                self._appt_update,
                APPT_POLL_PERIOD_S,
                context={"_id": chat_id, "user_data": user_data},
                name=job_name,
            )

            jobs = bot_data.get(DataId.JOBS.name, {})
            jobs[job.job.id] = {
                DataId.USER.name: user,
                DataId.CHAT_ID.name: chat_id,
            }
            bot_data[DataId.JOBS.name] = jobs

            logger.info(
                f"Finding appointments with settings {state_code} {postal_code:05d} {distance}mi"
            )

        return job

    def _remove_jobs(
        self, job_queue: tge.JobQueue, job_name: str, bot_data: dict, user_data: dict
    ):
        """
        Remove all search jobs from queue with job_name

        :param job_queue: Job Queue to query
        :param job_name: Name of jobs to remove
        :param bot_data: Bot Data to update (this is used to restart jobs on Bot restart)
        :return: None
        """
        jobs = job_queue.get_jobs_by_name(job_name)
        for job in jobs:
            job.schedule_removal()
            del bot_data[DataId.JOBS.name][job.job.id]
            user_data[DataId.APPT_FINDER.name] = None

    def _appt_update(self, context: tge.CallbackContext):
        """
        Get new appointments

        :param context: Context including chat id and user data
        """
        msg_txt = ""
        appt_finder = context.job.context["user_data"].get(DataId.APPT_FINDER.name)

        new_appts = []
        try:
            new_appts = appt_finder.get_new_appts()

        except requests.HTTPError:
            msg_txt = "Error getting new appointments from https://vaccinespotter.org; will try again shortly..."

        for appt in new_appts:
            # msg_appt = f"\n\n{appt}"
            # msg_appt = (
            #     "\n\n"
            #     f"<u><b>{html.escape(appt.mod_date.strftime('%H:%M %Z'))}</b></u>"
            #     "\n\n"
            #     f"<b>{html.escape(appt.brand)}</b>\n"
            #     f"{html.escape(appt.name)}\n"
            #     f"{html.escape(appt.street_address)}\n"
            #     f"{html.escape(appt.city)}, {html.escape(appt.state_code)} {html.escape(appt.postal_code)}\n"
            #     f"<i>{appt.distance:.2f}mi</i>"
            #     "\n\n"
            #     f'<a href="{html.escape(appt.url)}">Schedule Appointment</a>'
            # )
            mod_date = appt.mod_date.strftime("%H:%M %Z")
            brand = appt.brand
            name = appt.name
            address = appt.street_address
            city = appt.city
            state = appt.state_code
            postal_code = appt.postal_code
            distance = appt.distance

            mod_date = f"{mod_date}" if mod_date else "Unknown Time"
            brand = f"{brand}" if brand else "Unknown Brand"
            name = f"{name}" if name else "Unknown Name"
            address = f"{address}" if address else "Unknown Address"
            city = f"{city}" if city else "Unknown City"
            state = f"{state}" if state else "Unknown State"
            postal_code = f"{postal_code}" if postal_code else "Unknown Postal Code"
            distance = f"{distance:.2f}mi" if distance else ""

            msg_appt = (
                "\n\n"
                f"<u><b>{html.escape(mod_date)}</b></u>"
                "\n\n"
                f"<b>{html.escape(brand)}</b>\n"
                f"{html.escape(name)}\n"
                f"{html.escape(address)}\n"
                f"{html.escape(city)}, {html.escape(state)} {html.escape(postal_code)}\n"
                f"<i>{distance}</i>"
                "\n\n"
                f'<a href="{html.escape(appt.url)}">Schedule Appointment</a>'
            )

            # Cap message length; leave room for "truncated" message to user
            if (
                len(msg_txt) + len(msg_appt) + len(TG_MSG_TOO_LONG)
                <= TG_MSG_LIM_NUM_CHARS
            ):
                msg_txt += msg_appt

            else:
                if len(msg_txt) + len(TG_MSG_TOO_LONG) <= TG_MSG_LIM_NUM_CHARS:
                    msg_txt += TG_MSG_TOO_LONG

                break

        if len(msg_txt) > 0:
            context.bot.send_message(
                context.job.context["_id"], text=msg_txt, parse_mode="HTML"
            )


if __name__ == "__main__":
    cli_parser = argparse.ArgumentParser(
        description="Simple Telegram Bot for finding Covid-19 vaccine appointments"
    )
    cli_parser.add_argument("bot_token", help="Telegram Bot Token", type=str)

    cli_args = cli_parser.parse_args()

    persistent_storage = tge.PicklePersistence(filename="./nvbotdata")
    updater = tge.updater.Updater(
        token=cli_args.bot_token, persistence=persistent_storage
    )
    bot = CovidVacBot(updater)

    updater.start_polling()

    updater.idle()
