FROM python:3.9

WORKDIR /app

RUN useradd -ms /bin/bash tux

USER tux

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

CMD ["python3", "cv19spot-telegram-bot.py"]
