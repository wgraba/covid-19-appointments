from os import stat
from typing import Union
import requests
import geopy
import geopy.geocoders
import geopy.distance
import datetime
import dateutil
import dateutil.tz
import dateutil.parser
import timezonefinder
from .cache_utils import timed_lru_cache
import logging


logger = logging.getLogger(__name__)


class SimpleGeocoder:
    def __init__(self) -> None:
        self._geocoder = geopy.geocoders.Nominatim(user_agent="covid-appointments")

    def get_loc(self, postalcode: int) -> "Union[tuple[float, float], None]":
        """
        Get location by Postal Code

        :param postalcode: Postal Code
        :return: Tuple representing (latitidue, longitude) or None if location not found
        """
        ret_loc = None
        loc = self._geocoder.geocode({"postalcode": postalcode}, country_codes="us")
        if loc and isinstance(loc, geopy.Location):
            ret_loc = (loc.latitude, loc.longitude)

        return ret_loc


class Appointment:
    """
    Representation of an appointment

    :param brand: Provider brand name, e.g., Safeway
    :param name: Provider name, e.g., Safeway #117
    :param address: Street address or provider
    :param city: City of provider
    :param state_code: State of provider
    :param postal_code: Postal code of provider
    :param mod_date: Date appointment info was last modified
    :param url: URL of provider scheduling
    :param distance_mi: Distance in miles from reference
    :return: None
    """

    def __init__(
        self,
        _id: int,
        brand: str,
        name: str,
        address: str,
        city: str,
        state_code: str,
        postal_code: str,
        mod_date: datetime.datetime,
        url: str,
        distance_mi: int = None,
    ) -> None:
        self._id = _id
        self._brand = brand
        self._name = name
        self._address = address
        self._city = city
        self._state_code = state_code
        self._postal_code = postal_code
        self._mod_date = mod_date
        self._url = url
        self._distance_mi = distance_mi

    def __str__(self) -> str:
        return (
            f"{self._mod_date.strftime('%H:%M %Z')}\n\n"
            f"{self._brand}\n"
            f"{self._name}\n"
            f"{self._address}\n"
            f"{self._city}, {self._state_code} {self._postal_code}\n"
            f"{self._distance_mi:.2f}mi\n\n"
            f"{self._url}\n"
        )

    def __eq__(self, o: object) -> bool:
        if not isinstance(o, Appointment):
            raise TypeError(
                f"Unable to compare requested {type(o)} with expected {type(self)}"
            )

        return o._id == self._id and o._mod_date == self._mod_date

    @property
    def brand(self) -> str:
        """
        Provider brand name e.g. Safeway
        """
        return self._brand

    @property
    def name(self) -> str:
        """
        Location name e.g. Safeway #407
        """
        return self._name

    @property
    def street_address(self) -> str:
        return self._address

    @property
    def city(self) -> str:
        return self._city

    @property
    def state_code(self) -> str:
        return self._state_code

    @property
    def postal_code(self) -> str:
        return self._postal_code

    @property
    def mod_date(self) -> datetime.datetime:
        """
        Data appointment was modified

        :return: Timestamp
        """
        return self._mod_date

    @property
    def url(self) -> str:
        return self._url

    @property
    def distance(self) -> Union[float, None]:
        """
        Distance to appointment in miles.

        :return: Distance or None (no distance calculated)
        """
        return self._distance_mi


class StateApptData:
    """
    Data struture for basic state appointment data

    :param state_code: 2-letter State code
    :param state_name: Full State name
    :param num_locs: Number of locations providing vaccine
    :param nun_provs: Number of unique providers for vaccine
    """

    def __init__(
        self, state_code: str, state_name: str, num_locs: int, num_provs: int
    ) -> None:
        self._state_code = state_code
        self._state_name = state_name
        self._num_locs = num_locs
        self._num_provs = num_provs

    @property
    def state_code(self):
        return self._state_code

    @property
    def state_name(self):
        return self._state_name

    @property
    def num_locations(self):
        return self._num_locs

    @property
    def num_providers(self):
        return self._num_provs


APPT_POLL_PERIOD_LIMIT_S = 60  # Limit polling of https://vacspotter.org API


class VacSpotter:
    """
    Abstraction of https://vaccinespotter.org data. Provides time-limited
    caching
    """

    def __init__(self) -> None:
        self._last_check_date = None

    @property
    def last_check_date(self) -> Union[datetime.datetime, None]:
        return self._last_check_date

    @timed_lru_cache(APPT_POLL_PERIOD_LIMIT_S)
    def get_data(self, state_code: str) -> requests.Response:
        """
        Get appointments from vaccinespoter.org

        :return: Data for state
        """
        rsp = requests.get(
            f"https://www.vaccinespotter.org/api/v0/states/{state_code}.json"
        )

        self._last_check_date = datetime.datetime.now(dateutil.tz.gettz("UTC"))

        return rsp

    @timed_lru_cache(APPT_POLL_PERIOD_LIMIT_S * 10)
    def get_state_data(self, state_code: str) -> Union[StateApptData, None]:
        """
        Get general data about appointments by state -
        Exists, store count, unique providers, etc

        :return General appointment data or None if it doesn't exist
        """
        rsp = requests.get(f"https://www.vaccinespotter.org/api/v0/states.json")
        state_data = None
        for state in rsp.json():
            if state["code"] == state_code.upper():
                state_data = StateApptData(
                    state["code"],
                    state["name"],
                    state["store_count"],
                    state["provider_brand_count"],
                )

        return state_data


vac_spotter = VacSpotter()


class AppointmentFinder:
    """
    Find potentially open appointments based on location and distance

    :param state_code: 2-letter state code, e.g., CO
    :param max_distance_mi: Max acceptable distance in miles from from_coords
    :param from_coords: Tuple of (latitude, longitude) coordinates; used with distance to determine
                        acceptable appointments
    """

    def __init__(
        self, state_code: str, distance: float = None, postal_code: int = None
    ):
        if len(state_code) != 2:
            raise ValueError(f"Expected 2-letter state code; got f{state_code}")

        if distance and distance < 0:
            raise ValueError(f"Expected distance greater than 0; gpt {distance}")

        if postal_code and postal_code <= 0:
            raise ValueError(f"Expected postal code > 0; got {postal_code!r}")

        self._distance = distance
        self._state_code = state_code.upper()
        self._postal_code = postal_code
        if postal_code:
            self._from_coords = SimpleGeocoder().get_loc(postal_code)

        else:
            self._from_coords = None

        self._old_appts = []

    @property
    def distance(self) -> Union[float, None]:
        """
        Distance in miles
        """
        return self._distance

    @property
    def state_code(self) -> str:
        """
        Two-letter state code
        """
        return self._state_code

    @property
    def postal_code(self) -> Union[int, None]:
        """
        Postal Code (Zip Code in U.S.)
        """
        return self._postal_code

    @property
    def last_check_date(self) -> Union[datetime.datetime, None]:
        return vac_spotter.last_check_date

    def _get_data(self) -> requests.Response:
        """
        Get appointment from vaccinespoter.org

        :return: Data for state
        """
        return vac_spotter.get_data(self._state_code)

    @timed_lru_cache(int(APPT_POLL_PERIOD_LIMIT_S / 4))
    def get_appts(self) -> list:
        """
        Get acceptable appointment: open and within range (if applicable).

        :return: List of appointments
        """
        appts = []
        data = self._get_data()
        for feature in data.json()["features"]:
            to_coords = (
                feature["geometry"]["coordinates"][1],
                feature["geometry"]["coordinates"][0],
            )
            distance_mi = None
            if self._from_coords:
                # Skip if location is greater than desired distance
                distance_mi = abs(
                    geopy.distance.distance(self._from_coords, to_coords).miles
                )
                if self._distance and distance_mi > self._distance:
                    continue

            properties = feature["properties"]
            if properties.get("appointments_available"):
                tzone = dateutil.tz.gettz(
                    timezonefinder.TimezoneFinder().timezone_at(
                        lat=to_coords[0], lng=to_coords[1]
                    )
                )
                mod_date = dateutil.parser.parse(
                    properties["appointments_last_modified"]
                ).astimezone(tzone)
                appt = Appointment(
                    properties["id"],
                    properties["provider_brand_name"],
                    properties["name"],
                    properties["address"],
                    properties["city"],
                    properties["state"],
                    properties["postal_code"],
                    mod_date,
                    properties["url"],
                    distance_mi,
                )

                appts.append(appt)

        return appts

    def get_new_appts(self) -> list:
        """
        Get new appointments.
        Keeps track of old appointments for 1 day

        :return: List of new Appointments or an empty list if there are no new appointments
        """
        _dbg_num_new_appts = 0
        _dbg_num_old_appts_trim = 0

        new_appts = []
        appts = self.get_appts()
        for appt in appts:
            if appt not in self._old_appts:
                new_appts.append(appt)
                self._old_appts.append(appt)

                _dbg_num_new_appts += 1

        # Trim old appointments
        for appt in self._old_appts[:]:
            num_days = (
                datetime.datetime.now(dateutil.tz.gettz("UTC")) - appt.mod_date
            ).days
            if num_days >= 1:
                self._old_appts.remove(appt)

                _dbg_num_old_appts_trim += 1

        if _dbg_num_new_appts > 0:
            logger.info(f"Added {_dbg_num_new_appts} new appointments")

        if _dbg_num_old_appts_trim:
            logger.info(f"Trimmed {_dbg_num_old_appts_trim} old appointments")

        return new_appts